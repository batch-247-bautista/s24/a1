//console.log('Hellow World?');

// [SECTION] Exponent Operator

	const firstNum = 2 ** 3;
	//console.log(firstNum);

	const Message = `The cube of 2 is ${firstNum}.`
	console.log(Message);

	const fullAddress = ['258', '90011'];

	// Pre-Array Destructuring
	//console.log(fullAddress[0]);
	//console.log(fullAddress[1]);
	

	console.log(`I live at ${fullAddress[0]} Washington Ave NW, California ${fullAddress[1]}`);


	const reptile = {
		givenName: 'Lolong',
		breed: 'salwater crocodile',
		weight: '1075 kgs.',
		length: '20 ft 3 in.',
	};

	// Pre-Object Desctructuring
	//console.log(reptile.givenName);
	//console.log(reptile.breed);
	//console.log(reptile.weight);
	//console.log(reptile.length);

	console.log(` ${reptile.givenName} is a  ${reptile.breed}. He weighed ${reptile.weight} with a mesurement of ${reptile.length}.`)





	const numbers = ['1', '2', '3', '4', '5'];

	// Pre-Arrow Function
	numbers.forEach(function(number){
		console.log(`${number}`);
	});

	let iteration = 0;

				//console.log(numbers);
				let numbers1 = [1, 2, 3, 4, 5 ];

				let reducedArray = numbers1.reduce(function(x,y){
					//console.log('Current iiteration:' + ++ iteration);
					//console.log('accumulator:' + x);
					//console.log('currentValue:' + y);

					return x + y;
				});

				console.log( + reducedArray);



		class Dog {
				constructor(name, age, breed){
					this.name = name;
					this.age = age;
					this.breed = breed;
				}
			}
		

		const myDog = new Dog()
		

		// Values of properties may be assigned after creation/instantiation of an object

			myDog.name = 'Frankie';
			myDog.age = 5;
			myDog.breed = 'Miniature Dachshund';

			console.log(myDog);
	